/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skillDataPlugin

import com.google.gson.reflect.TypeToken
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.asClassName
import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException
import java.io.File
import java.io.FileReader


/**
 * Reads JSON files from `app/src/main/skill_data` and generate
 * Kotlin files in `app/build/generated/skill_data_plugin`.
 */
open class SkillDataTask : DefaultTask() {
    @InputDirectory
    val inputDir: DirectoryProperty = project.objects.directoryProperty().apply {
        set(project.file("src/main/skill_data"))
    }

    @OutputDirectory
    val outputDir: DirectoryProperty = project.objects.directoryProperty().apply {
        set(project.file("${project.buildDir}/generated/skill_data_plugin"))
    }

    @TaskAction
    fun generateResource() {
        val gson = com.google.gson.Gson()
        val inputDirFile = inputDir.get().asFile
        val outputDirFile = outputDir.get().asFile

        // read the skills.json file
        val skills = FileReader(File(inputDirFile, "skills.json"))
            .use { gson.fromJson(it, SKILLS_JSON_TYPE) }
            .map { it.toSkillData() }
        val languages = ArrayList<String>()
        var invalidCapturesFound = false

        // Read skill examples in every skill_id.json file in every $language/ folder.
        // Note: in the folder structure, examples are first split by language (due to the various
        // $language/ folders), and then by skill id (hence the various skill_id.json files), while
        // in the generated Kotlin code it will be the opposite way around.
        // Note: the language ids are simply the $language/ folder names.
        for (lang in inputDirFile.listFiles { file -> file.isDirectory }!!) {
            var langHasSkill = false
            for (skill in skills) {
                // try to read files only for skills defined in skills.json
                val file = File(lang, skill.id + JSON_EXT)
                if (!file.exists()){
                    continue
                }
                langHasSkill = true
                val examples = FileReader(file).use { gson.fromJson(it, EXAMPLES_JSON_TYPE) }

                // make sure all of the captures specified in the example are correctly spelled
                for (captures in examples.values) {
                    for (capture in captures.keys) {
                        if (!skill.captures.contains(capture)) {
                            invalidCapturesFound = true // crash later, first collect all errors
                            System.err.println("Invalid capture \"$capture\" for skill " +
                                    "\"${skill.id}\" in language \"${lang.name}\" (known " +
                                    "captures: ${skill.captures.joinToString()}) at ${file.path}")
                        }
                    }
                }

                // kotlin code block generation for all skill examples in the current language
                val codeBlock = CodeBlock.builder()
                    .add(
                        "listOf(${
                            examples
                                .map { example ->
                                    "SkillExample(%S, %S${
                                        ", %S to %S".repeat(example.value.size)
                                    })"
                                }
                                .joinToString()
                        })",
                        args = examples.flatMap { example ->
                            return@flatMap sequenceOf(skill.id, example.key) +
                                    example.value.flatMap {
                                            capture -> listOf(capture.key, capture.value)
                                    }
                        }.toTypedArray()
                    )

                skill.languages[lang.name] = codeBlock.build()
            }

            if (langHasSkill) {
                languages.add(lang.name)
            }
        }

        // generate a file for each skill, containing a map with the collected examples grouped
        // by language
        for (skill in skills) {
            skill.kotlinFile.addImport("foundation.e.assistant.skill", "SkillExample")

            skill.kotlinFile.addType(
                TypeSpec.objectBuilder(skill.id)
                    .addProperty(PropertySpec.builder(
                        name = "llmData",
                        type = ClassName("foundation.e.assistant.skill", "SkillLlmData"),
                    )
                        .initializer(
                            "SkillLlmData(id = %S, captures = listOf(${
                                "%S,".repeat(skill.captures.size)
                            }), localeExamplesMap = mapOf(${
                                "%S to %L,".repeat(skill.languages.size)
                            }))",
                            args = listOf(
                                listOf(skill.id),
                                skill.captures,
                                skill.languages.entries.flatMap { (a, b) -> listOf(a, b) }
                            ).flatten().toTypedArray()
                        )
                        .build())
                    .apply {
                        for (capture in skill.captures) {
                            addProperty(PropertySpec.builder(
                                name = capture,
                                type = String::class,
                            )
                                .initializer("%S", capture)
                                .build())
                        }
                    }
                    .build()
            )

            skill.kotlinFile.build().writeTo(outputDirFile)

            if (invalidCapturesFound) {
                throw TaskExecutionException(this, Exception("Some captures were invalid"))
            }
        }

        // generate a SkillData.kt file with all non-empty languages found
        FileSpec.builder(PACKAGE_NAME, CLASS_NAME)
            .addFileComment(FILE_COMMENT)
            .addProperty(PropertySpec
                .builder("languages",
                    List::class.asClassName().parameterizedBy(String::class.asClassName())
                )
                .initializer(
                    "listOf(${"%S,".repeat(languages.size)})",
                    args = languages.toTypedArray()
                )
                .build()
            )
            .build()
            .writeTo(outputDirFile)
    }

    companion object {
        private val EXAMPLES_JSON_TYPE = object : TypeToken<Map<String, Map<String, String>>>() {}
        private val SKILLS_JSON_TYPE = object : TypeToken<List<SkillDataGson>>() {}
    }
}
