/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skillDataPlugin

import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec


/**
 * The contents of the root `skills.json` file.
 */
data class SkillDataGson(
    val id: String,
    val captures: List<String>,
) {
    fun toSkillData(): SkillData {
        return SkillData(
            id = id,
            captures = captures,
        )
    }
}

/**
 * The contents of the root `skills.json` file, with additional fields that help with loading data
 * from other `language/skill_id.json` files.
 */
data class SkillData(
    val id: String,
    val captures: List<String>,
) {
    val kotlinFile = FileSpec.builder(PACKAGE_NAME, CLASS_NAME + "_" + id)
        .addFileComment(FILE_COMMENT)
    val languages = HashMap<String, CodeBlock>()
}
