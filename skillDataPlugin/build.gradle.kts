/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


group = "foundation.e.assistant.skillDataPlugin"

plugins {
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        create("skill_data_plugin") {
            id = "foundation.e.assistant.skillDataPlugin"
            implementationClass = "foundation.e.assistant.skillDataPlugin.SkillDataPlugin"
        }
    }
}

dependencies {
    // these dependencies are usually compile-time dependencies, but since this is a plugin, we want
    // to access the gradle libraries at the runtime of the plugin, which happens at compile-time
    // for the app
    implementation(libs.android.tools.build.gradle)
    implementation(libs.kotlin.gradle.plugin)
    implementation(libs.kotlinpoet)
    implementation(libs.gson)
}
