/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.FabPosition
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.QuestionAnswer
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import foundation.e.assistant.R
import foundation.e.assistant.input.VoskFab
import foundation.e.assistant.item.ErrorItem
import foundation.e.assistant.item.MessageItem
import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.llm.LlmSelector
import foundation.e.assistant.util.SearchTopAppBar

@Composable
fun MainScreen() {
    val mainScreenViewModel: MainScreenViewModel = viewModel()
    val interactions = mainScreenViewModel.interactionsState.collectAsState().value
    val temporaryInputText = mainScreenViewModel.temporaryInputText.collectAsState().value
    val temporaryLlmText = mainScreenViewModel.temporaryLlmText.collectAsState().value
    val loadingSkillResponse = mainScreenViewModel.loadingSkillResponse.collectAsState().value

    Scaffold(
        topBar = {
            SearchTopAppBar(
                title = stringResource(R.string.app_name),
                hint = stringResource(R.string.ask_me_anything),
                backgroundColor = MaterialTheme.colors.primary,
                onSubmitSearch = mainScreenViewModel::submitTextInputEvent,
                navigationIcon = {
                     IconButton(onClick = { /*TODO*/ }) {
                         Icon(
                             imageVector = Icons.Filled.Settings,
                             contentDescription = stringResource(R.string.settings),
                         )
                     }
                },
                searchIconContent = {
                    Icon(
                        imageVector = Icons.Filled.QuestionAnswer,
                        contentDescription = stringResource(R.string.ask_me_anything),
                    )
                },
            )
        },
        floatingActionButton = {
            VoskFab()
        },
        floatingActionButtonPosition = FabPosition.Center,
        content = { paddingValues ->
            Column(
                modifier = Modifier.padding(paddingValues),
            ) {
                LlmSelector()
                MessageList(
                    interactions = interactions,
                    temporaryInputText = temporaryInputText,
                    temporaryLlmText = temporaryLlmText,
                    loadingSkillResponse = loadingSkillResponse,
                )
            }
        },
    )
}

@Composable
fun MessageList(
    interactions: List<RenderableItem>,
    temporaryInputText: String?,
    temporaryLlmText: String?,
    loadingSkillResponse: Boolean,
) {
    val listState = rememberLazyListState()

    LaunchedEffect(interactions) {
        if (
            // scroll to the bottom when the user just provided some new input
            (interactions.lastOrNull()?.let { isInteractionSourceUser(it) } == true) ||
            // and keep auto-scrolled to the bottom when the list is already scrolled to the bottom
            (listState.layoutInfo.visibleItemsInfo.lastOrNull()?.index ==
                    listState.layoutInfo.totalItemsCount - 1)
        ) {
            listState.animateScrollToItem(listState.layoutInfo.totalItemsCount)
        }
    }

    LazyColumn(state = listState) {
        items(interactions.size) { i ->
            MessageBox(interactions[i])
        }
        if (temporaryInputText != null) {
            item {
                MessageBox(
                    MessageItem(
                        text = temporaryInputText,
                        isFinal = false,
                        sourceIsUser = true,
                    )
                )
            }
        }
        if (temporaryLlmText != null) {
            item {
                MessageBox(
                    MessageItem(
                        text = temporaryLlmText,
                        // the temporaryLlmText will remain != null even when the LLM has finished
                        // generating, but the skill is still processing
                        isFinal = loadingSkillResponse,
                        sourceIsUser = false,
                    )
                )
            }
        }
        item {
            Spacer(modifier = Modifier.height(88.dp))
        }
    }
}

@Composable
fun MessageBox(interaction: RenderableItem) {
    //val clipboardManager = LocalClipboardManager.current
    val sourceIsUser = isInteractionSourceUser(interaction)

    Surface(
        modifier = Modifier
            .padding(
                top = 4.dp,
                bottom = 4.dp,
                end = 8.dp,
                start = 8.dp,
            )
            .fillMaxWidth(),
        color = MaterialTheme.colors.run { if (sourceIsUser) secondary else surface },
        contentColor = MaterialTheme.colors.run { if (sourceIsUser) onSecondary else onSurface },
        shape = RoundedCornerShape(12.dp),
        elevation = 2.dp,
    ) {
        Box(modifier = Modifier.padding(12.dp)) {
            interaction.Render()
        }
    }
}

fun isInteractionSourceUser(interaction: RenderableItem): Boolean {
    return when(interaction) {
        is MessageItem -> interaction.sourceIsUser
        is ErrorItem -> interaction.sourceIsUser
        else -> false
    }
}
