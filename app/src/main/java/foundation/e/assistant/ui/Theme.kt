/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.ui

import android.app.Activity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Typography
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.colorResource
import androidx.core.view.WindowCompat


/**
 * Uses colors from [foundation.e.elib.R], that is, /e/'s SDK. Returns a Material 2 theme.
 */
@Composable
fun AppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    // TODO a designer should setup colors properly
    val colors = if (darkTheme) darkColors(
        primary = colorResource(foundation.e.elib.R.color.e_action_bar_dark),
        primaryVariant = colorResource(foundation.e.elib.R.color.e_action_bar_dark),
        secondary = colorResource(foundation.e.elib.R.color.e_accent_dark),
        //secondaryVariant = ,
        background = colorResource(foundation.e.elib.R.color.e_background_dark),
        surface = colorResource(foundation.e.elib.R.color.e_floating_background_dark),
        error = Color(0xFFCF6679),
        onPrimary = colorResource(foundation.e.elib.R.color.e_primary_text_color_dark),
        onSecondary = colorResource(foundation.e.elib.R.color.e_primary_text_color_light),
        onBackground = colorResource(foundation.e.elib.R.color.e_primary_text_color_dark),
        onSurface = colorResource(foundation.e.elib.R.color.e_primary_text_color_dark),
        onError = Color.Black,
    ) else lightColors(
        primary = colorResource(foundation.e.elib.R.color.e_action_bar_light),
        primaryVariant = colorResource(foundation.e.elib.R.color.e_action_bar_light),
        secondary = colorResource(foundation.e.elib.R.color.e_accent_light),
        //secondaryVariant = ,
        background = colorResource(foundation.e.elib.R.color.e_background_light),
        surface = colorResource(foundation.e.elib.R.color.e_floating_background_light),
        error = Color(0xFFB00020),
        onPrimary = colorResource(foundation.e.elib.R.color.e_primary_text_color_light),
        onSecondary = colorResource(foundation.e.elib.R.color.e_primary_text_color_dark),
        onBackground = colorResource(foundation.e.elib.R.color.e_primary_text_color_light),
        onSurface = colorResource(foundation.e.elib.R.color.e_primary_text_color_light),
        onError = Color.White,
    )

    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colors.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = !darkTheme
        }
    }

    MaterialTheme(
        colors = colors,
        typography = Typography(),
        content = content
    )
}
