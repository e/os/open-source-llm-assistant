/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.assistant.util

import retrofit2.HttpException
import retrofit2.Response

sealed interface ApiResult<T : Any>

class ApiSuccess<T : Any>(val data: T) : ApiResult<T>

class ApiError<T : Any>(val code: Int, val message: String?) : ApiResult<T>

class ApiException<T : Any>(val e: Throwable) : ApiResult<T>

/**
 * Obtains the response from the provided lambda, executes, and maps any errors into either
 * [ApiError] in case of HTTP errors, or [ApiException] in case of unhandled network exceptions.
 * Otherwise returns an [ApiSuccess] with the data.
 */
suspend fun <T : Any> handleApi(execute: suspend () -> Response<T>): ApiResult<T> {
    // we need to catch all exceptions thrown by the api, and we can't know the type more precisely
    @Suppress("detekt:exceptions:TooGenericExceptionCaught")
    return try {
        val response = execute()
        val body = response.body()

        if (response.isSuccessful && body != null) {
            ApiSuccess(body)
        } else {
            ApiError(code = response.code(), message = response.message())
        }
    } catch (e: HttpException) {
        ApiError(code = e.code(), message = e.message())
    } catch (e: Throwable) {
        ApiException(e)
    }
}
