/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.util

import android.view.KeyEvent
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.material.ContentAlpha
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalContentColor
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TextFieldDefaults.indicatorLine
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.takeOrElse
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import foundation.e.assistant.R
import foundation.e.assistant.ui.AppTheme

@Composable
fun AppBarTitle(text: String) {
    Text(
        text,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )
}

/**
 * A text field in the app bar, making sure all colors are correct, focus is handled correctly,
 * and the keyboard open/close/send events are received.
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AppBarTextField(
    value: String,
    onValueChange: (String) -> Unit,
    hint: String,
    modifier: Modifier = Modifier,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
) {
    val interactionSource = remember { MutableInteractionSource() }

    // If color is not provided via the text style, use content color as a default
    val textStyle = LocalTextStyle.current
    val textColor = textStyle.color.takeOrElse { LocalContentColor.current }
    val mergedTextStyle = textStyle.merge(TextStyle(color = textColor))

    // make sure the decoration box colors fit well in the app bar and has no background
    // (values partially copied from textFieldColors() default parameters)
    val colors = TextFieldDefaults.textFieldColors(
        textColor = textColor,
        backgroundColor = Color.Transparent,
        cursorColor = textColor,
        focusedIndicatorColor = textColor.copy(ContentAlpha.high),
        unfocusedIndicatorColor = textColor.copy(TextFieldDefaults.UnfocusedIndicatorLineOpacity),
        focusedLabelColor = textColor.copy(ContentAlpha.high),
        unfocusedLabelColor = textColor.copy(ContentAlpha.medium),
        placeholderColor = textColor.copy(ContentAlpha.medium),
    )

    // request focus when this composable is first initialized
    val focusRequester = FocusRequester()
    SideEffect {
        focusRequester.requestFocus()
    }

    // set the correct cursor position when this composable is first initialized
    var textFieldValueState by remember {
        mutableStateOf(TextFieldValue(value, TextRange(value.length)))
    }
    val textFieldValue = textFieldValueState.copy(text = value) // keep the value updated

    // copied from the BasicTextField implementation that takes a String
    SideEffect {
        if (textFieldValue.selection != textFieldValueState.selection ||
            textFieldValue.composition != textFieldValueState.composition) {
            textFieldValueState = textFieldValue
        }
    }

    CompositionLocalProvider(
        LocalTextSelectionColors provides LocalTextSelectionColors.current
    ) {
        BasicTextField(
            value = textFieldValue,
            onValueChange = {
                textFieldValueState = it
                // remove newlines to avoid strange layout issues, and also because singleLine=true
                onValueChange(it.text.replace("\n", ""))
            },
            modifier = modifier
                .fillMaxWidth()
                .heightIn(32.dp)
                .indicatorLine(
                    enabled = true,
                    isError = false,
                    interactionSource = interactionSource,
                    colors = colors,
                )
                .focusRequester(focusRequester),
            textStyle = mergedTextStyle,
            cursorBrush = SolidColor(textColor),
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            interactionSource = interactionSource,
            singleLine = true,
            decorationBox = { innerTextField ->
                // places text field with placeholder and appropriate bottom padding
                TextFieldDefaults.TextFieldDecorationBox(
                    value = value,
                    innerTextField = innerTextField,
                    enabled = true,
                    singleLine = true,
                    visualTransformation = VisualTransformation.None,
                    interactionSource = interactionSource,
                    isError = false,
                    placeholder = {
                        Text(text = hint)
                    },
                    colors = colors,
                    contentPadding = PaddingValues(bottom = 4.dp),
                )
            }
        )
    }
}


/**
 * An app bar with a customizable search icon that opens a text field in the app bar.
 */
@Composable
fun SearchTopAppBar(
    title: String,
    hint: String,
    backgroundColor: Color,
    onSubmitSearch: (String) -> Unit,
    navigationIcon: @Composable () -> Unit,
    searchIconContent: @Composable () -> Unit,
) {
    var searchExpanded by rememberSaveable { mutableStateOf(false) }
    var searchString by rememberSaveable { mutableStateOf("") }

    if (searchExpanded) {
        SearchTopAppBarExpanded(
            searchString = searchString,
            setSearchString = {
                if (searchExpanded) {
                    searchString = it
                }
            },
            hint = hint,
            backgroundColor = backgroundColor,
            onSearchDone = { finalSearchString ->
                searchExpanded = false
                if (finalSearchString.isNotEmpty()) {
                    onSubmitSearch(finalSearchString)
                }
            }
        )
    } else {
        SearchTopAppBarUnexpanded(
            title = title,
            backgroundColor = backgroundColor,
            onSearchClick = { searchExpanded = true },
            navigationIcon = navigationIcon,
            searchIconContent = searchIconContent,
        )
    }
}

@Preview
@Composable
private fun SearchTopAppBarPreview() {
    AppTheme {
        SearchTopAppBar(
            title = "The title",
            hint = "The hint…",
            backgroundColor = MaterialTheme.colors.primary,
            onSubmitSearch = {},
            navigationIcon = {
                IconButton(onClick = { }) {
                    Icon(
                        imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                        contentDescription = ""
                    )
                }
            },
            searchIconContent = {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = ""
                )
            }
        )
    }
}

@Composable
private fun SearchTopAppBarUnexpanded(
    title: String,
    backgroundColor: Color,
    onSearchClick: () -> Unit,
    navigationIcon: @Composable () -> Unit,
    searchIconContent: @Composable () -> Unit,
) {
    TopAppBar(
        title = {
            AppBarTitle(text = title)
        },
        navigationIcon = navigationIcon,
        actions = {
            IconButton(
                onClick = onSearchClick,
                content = searchIconContent,
            )
        },
        backgroundColor = backgroundColor,
    )
}

@Composable
private fun SearchTopAppBarExpanded(
    searchString: String,
    setSearchString: (String) -> Unit,
    hint: String,
    backgroundColor: Color,
    onSearchDone: (String) -> Unit
) {
    fun exitSearch(finalSearchString: String) {
        // clear the search filter
        setSearchString("")
        // then close the search bar
        onSearchDone(finalSearchString)
    }

    TopAppBar(
        title = {
            AppBarTextField(
                value = searchString,
                onValueChange = setSearchString,
                hint = hint,
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                keyboardActions = KeyboardActions(onSearch = { exitSearch(searchString) }),
                modifier = Modifier.onKeyEvent {
                    if (it.nativeKeyEvent.keyCode == KeyEvent.KEYCODE_ENTER) {
                        exitSearch(searchString)
                        return@onKeyEvent true
                    }
                    return@onKeyEvent false
                },
            )
        },
        navigationIcon = {
            IconButton(onClick = { exitSearch("") }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = stringResource(R.string.back)
                )
            }
        },
        actions = {
            IconButton(onClick = { setSearchString("") }) {
                Icon(
                    imageVector = Icons.Filled.Clear,
                    contentDescription = stringResource(R.string.clear)
                )
            }
        },
        backgroundColor = backgroundColor,
    )

    BackHandler {
        // this will only be triggered if the user presses back when the keyboard is already closed
        exitSearch("")
    }
}
