/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.util

import java.util.Locale

/**
 * @return a concatenation of [strings] separated by " • ".
 */
fun concatStringsForUi(vararg strings: String?): String {
    return strings.filter { it?.isNotEmpty() == true }.joinToString(separator = " • ")
}

/**
 * @param locale the current user locale
 * @return the whole [this] lowercase, with only the first letter uppercase.
 */
fun String.lowercaseCapitalized(locale: Locale): String {
    return lowercase(locale).replaceFirstChar { it.titlecase(locale) }
}
