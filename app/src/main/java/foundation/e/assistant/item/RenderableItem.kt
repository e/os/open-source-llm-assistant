/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.item

import androidx.compose.runtime.Composable

/**
 * Represents an item that can be shown in the output screen, and is also the output produced by
 * skills. Implementors of this interface should try to use a `data class`, if possible, or to at
 * least have a stable state that can be saved to disk, since in the future we may want to save and
 * restore previous user interactions with the assistant.
 */
interface RenderableItem {

    /**
     * A composable function that renders the UI based on the state contained in this
     * [RenderableItem] object. If possible avoid using view models or other ways to generate new
     * state inside this function, i.e. the loading of data should happen *before* instantiating a
     * [RenderableItem], and then the data should be stored inside the [RenderableItem] itself. If
     * for some reason you do need to load more data or handle mutable state e.g. for an interactive
     * widget, make sure to persist that data in the [RenderableItem] object, to make sure it can
     * be saved to disk and restored from there, in case in the future we'll implement saving and
     * restoring previous user interactions with the assistant.
     */
    @Composable
    fun Render()
}
