/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.item

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import androidx.compose.ui.tooling.preview.datasource.LoremIpsum
import foundation.e.assistant.ui.AppTheme

/**
 * Should *not* be used by skills directly, but is rather used to display user input and LLM output.
 */
data class MessageItem(
    val text: String,
    val isFinal: Boolean,
    val sourceIsUser: Boolean,
) : RenderableItem {
    @Composable
    override fun Render() {
        CompositionLocalProvider(
            LocalContentAlpha provides if (isFinal) ContentAlpha.high else ContentAlpha.medium
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // either the LLM has not generated any text yet (i.e. text.isBlank()), or the
                // LLM has finished generating text, but the skill response is not there yet
                // (i.e. isFinal)
                val showLoadingIndicator = !sourceIsUser && (text.isBlank() || isFinal)

                if (text.isNotBlank() || !showLoadingIndicator) {
                    // have the text use up some space in case there is no loading indicator
                    Text(
                        text = text,
                        textAlign = if (sourceIsUser) TextAlign.Right else TextAlign.Left,
                        modifier = Modifier.fillMaxWidth()
                    )
                }

                if (showLoadingIndicator) {
                    CircularProgressIndicator(color = LocalContentColor.current)
                }
            }
        }
    }
}

private const val PREVIEW_SHORT_TEXT = "short text"
@Suppress("detekt:style:MagicNumber")
private class MessageItemParamProv : CollectionPreviewParameterProvider<MessageItem>(listOf(
    MessageItem(PREVIEW_SHORT_TEXT, isFinal = false, sourceIsUser = false),
    MessageItem(PREVIEW_SHORT_TEXT, isFinal = false, sourceIsUser = true),
    MessageItem(PREVIEW_SHORT_TEXT, isFinal = true, sourceIsUser = false),
    MessageItem(PREVIEW_SHORT_TEXT, isFinal = true, sourceIsUser = true),
    MessageItem(LoremIpsum().values.first().substring(0..300), isFinal = false, sourceIsUser = false),
    MessageItem(LoremIpsum().values.first().substring(0..300), isFinal = false, sourceIsUser = true),
    MessageItem(LoremIpsum().values.first().substring(0..300), isFinal = true, sourceIsUser = false),
    MessageItem(LoremIpsum().values.first().substring(0..300), isFinal = true, sourceIsUser = true),
))

@Preview
@Composable
fun MessageItemPreview(@PreviewParameter(MessageItemParamProv::class) messageItem: MessageItem) {
    AppTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            messageItem.Render()
        }
    }
}
