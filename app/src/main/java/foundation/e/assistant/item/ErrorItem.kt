/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.item

import androidx.compose.material.Text
import androidx.compose.runtime.Composable

/**
 * Should *not* be used by skills directly, but is rather used to display errors happened while
 * getting user input, while evaluating the LLM, and while evaluating a skill.
 */
data class ErrorItem(
    val throwable: Throwable,
    val pendingInputOrOutput: String?,
    val sourceIsUser: Boolean,
) : RenderableItem {
    @Composable
    override fun Render() {
        Text(
            text = "Error getting ${if (sourceIsUser) "input" else "LLM output"}: $throwable" +
                    (pendingInputOrOutput?.let { " (pending message: $it)" } ?: "")
        )
    }
}
