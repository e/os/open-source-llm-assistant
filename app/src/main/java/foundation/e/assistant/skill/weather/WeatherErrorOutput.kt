/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.weather

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import foundation.e.assistant.R
import foundation.e.assistant.item.RenderableItem

data class WeatherErrorOutput(
    val place: String,
    val throwable: Throwable?,
    val code: Int?,
    val message: String?,
) : RenderableItem {
    @Composable
    override fun Render() {
        Text(
            text = if (code == 404) {
                stringResource(R.string.weather_could_not_find_city, place)
            } else if (code != null) {
                if (message == null) {
                    stringResource(R.string.http_error_code, code)
                } else {
                    stringResource(R.string.http_error_code_message, code, message)
                }
            } else {
                stringResource(R.string.api_error, "$throwable")
            }
        )
    }
}
