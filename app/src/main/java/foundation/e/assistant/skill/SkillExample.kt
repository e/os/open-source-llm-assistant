/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill

/**
 * An example that can be fed to an LLM so that it can deduce the skillId and captures for similar
 * sentences.
 *
 * For example this is a part of the prompt to an LLM with an example:
 * ```json
 * User: $query
 * LLM: { "skill": "$skillId", "$capture1key": "$capture1value", "$capture2k": "$capture2v", ... }
 * ```
 */
data class SkillExample(
    /**
     * The skill this example belongs to, e.g. `"weather"`.
     */
    val skillId: String,
    /**
     * An example query that the user may ask, e.g. `"what's the weather in Rome"`.
     */
    val query: String,
    /**
     * Some pairs of `(capture key, capture value)` referred to the example query, e.g.
     * `{"place": "Rome"}`.
     */
    val captures: Map<String, String>,
) {
    constructor(skillId: String, text: String, vararg captures: Pair<String, String>) :
        this(skillId, text, mapOf(pairs = captures))
}
