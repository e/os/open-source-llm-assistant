/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.weather.ipinfo

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import foundation.e.assistant.skill.weather.ipinfo.api.IpInfoApi
import foundation.e.assistant.skill.weather.ipinfo.api.IpInfoRepository
import foundation.e.assistant.skill.weather.ipinfo.api.IpInfoRepositoryImpl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier @Retention(AnnotationRetention.BINARY) annotation class IpInfoRetrofit

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Singleton
    @Provides
    @IpInfoRetrofit
    fun provideBaseRetrofit(client: OkHttpClient, gson: Gson): Retrofit =
        Retrofit.Builder()
            .baseUrl(IpInfoApi.BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()

    @Singleton
    @Provides
    fun provideBaseApi(@IpInfoRetrofit retrofit: Retrofit): IpInfoApi =
        retrofit.create(IpInfoApi::class.java)

    @Singleton
    @Provides
    fun provideRepository(baseApi: IpInfoApi): IpInfoRepository =
        IpInfoRepositoryImpl(baseApi)
}
