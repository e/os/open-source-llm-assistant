/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.weather

import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.skill.Skill
import foundation.e.assistant.skill.SkillContext
import foundation.e.assistant.skill.weather.ipinfo.api.IpInfoRepository
import foundation.e.assistant.skill.weather.openweathermap.api.OpenWeatherRepository
import foundation.e.assistant.skill.weather.openweathermap.models.Coordinate
import foundation.e.assistant.skill_data.weather
import foundation.e.assistant.util.ApiError
import foundation.e.assistant.util.ApiException
import foundation.e.assistant.util.ApiSuccess
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class WeatherSkill @Inject constructor(
    private val owmRepo: OpenWeatherRepository,
    private val ipInfoRepo: IpInfoRepository,
) : Skill {
    override val llmData = weather.llmData

    @Suppress("detekt:style:ReturnCount")
    override suspend fun computeOutput(
        userInput: String,
        captures: Map<String, String>,
        ctx: SkillContext,
    ): RenderableItem {
        // TODO pass the language from ctx.locale to requests to OWM

        val capturesPlace = captures[weather.place]
        val (place, lat, lon) = if (capturesPlace.isNullOrBlank()) {
            when (val ipInfo = ipInfoRepo.getIpInfo()) {
                is ApiError ->
                    return WeatherErrorOutput("", null, ipInfo.code, ipInfo.message)
                is ApiException ->
                    return WeatherErrorOutput("", ipInfo.e, null, null)
                is ApiSuccess -> Triple(ipInfo.data.city, ipInfo.data.lat, ipInfo.data.lon)
            }
        } else {
            when (val coords = owmRepo.getLocationCoordsByName(capturesPlace)) {
                is ApiError ->
                    return WeatherErrorOutput(capturesPlace, null, coords.code, coords.message)
                is ApiException ->
                    return WeatherErrorOutput(capturesPlace, coords.e, null, null)
                // coords.data[0].name is not a precise indication, so use capturesPlace directly
                is ApiSuccess -> Triple(capturesPlace, coords.data[0].lat, coords.data[0].lon)
            }
        }

        val (current, forecast) = coroutineScope {
            val currentAsync = async { owmRepo.getWeatherByCoords(Coordinate(lat, lon)) }
            val forecastAsync = async { owmRepo.getDaysForecastByCoords(Coordinate(lat, lon)) }
            Pair(currentAsync.await(), forecastAsync.await())
        }

        val currentData = when (current) {
            is ApiError -> return WeatherErrorOutput(place, null, current.code, current.message)
            is ApiException -> return WeatherErrorOutput(place, current.e, null, null)
            is ApiSuccess -> current.data
        }

        val forecastData = when (forecast) {
            is ApiError -> return WeatherErrorOutput(place, null, forecast.code, forecast.message)
            is ApiException -> return WeatherErrorOutput(place, forecast.e, null, null)
            is ApiSuccess -> forecast.data.list
        }

        return WeatherOutput(place, currentData, forecastData)
    }
}
