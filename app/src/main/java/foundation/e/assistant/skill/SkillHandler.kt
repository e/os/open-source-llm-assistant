/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill

import android.util.Log
import foundation.e.assistant.di.LocaleManager
import foundation.e.assistant.item.ErrorItem
import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.llm.model.PromptFormat
import foundation.e.assistant.skill.calendar.CalendarSkill
import foundation.e.assistant.skill.open.OpenSkill
import foundation.e.assistant.skill.summary.SummarySkill
import foundation.e.assistant.skill.weather.WeatherSkill
import org.json.JSONException
import org.json.JSONObject
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Takes care of forging LLM prompts from skill examples, and then extracting skill id and captures
 * from the LLM response, and finally evaluating the corresponding skill. For example, if the user
 * asks `"What's the weather in Rome"` and the LLM responds with
 * `{"skill":"weather","place":"Rome"}`, the SkillHandler should call the weather skill with the
 * following captures: `{"place":"Rome"}`
 */
@Singleton
class SkillHandler @Inject constructor(
    private val skillContext: SkillContext,
    private val localeManager: LocaleManager,
    weatherSkill: WeatherSkill,
) {
    private val skills = mapOf(
        weatherSkill.let { it.llmData.id to it },
        SummarySkill().let { it.llmData.id to it },
        CalendarSkill().let { it.llmData.id to it },
        OpenSkill().let { it.llmData.id to it },
    )

    /**
     * Gets a non-shuffled list of all skill examples.
     */
    fun getAllExamples(): List<SkillExample> {
        return skills.values.flatMap { it.llmData.getExamples(localeManager.localeString) }
    }

    /**
     * Get a skill by its [id].
     */
    fun getSkill(id: String): Skill? {
        return skills[id]
    }

    /**
     * Formats the LLM prompt with all examples from all skills and example outputs in JSON form (if
     * enabled), and finally appends the user input. Uses a SYSTEM prompt that suggests the model to
     * write in JSON.
     *
     * @param promptFormat the LLM prompt format to use
     * @param includeExamples whether to include examples. Feeding the LLM with a lot of examples
     * before seeing the user input gives a slower Time To First Token, but is necessary for LLMs
     * that were not fine-tuned for outputting JSON with the detected skill and captures.
     * @param userInput the user query to put at the end of the prompt
     */
    fun formatLlmPrompt(
        promptFormat: PromptFormat,
        includeExamples: Boolean,
        userInput: String
    ): String {
        val examples = if (includeExamples)
            getAllExamples().shuffled().map { example ->
                example.query to (
                        JSONObject()
                            .put("skill", example.skillId)
                            .apply {
                                example.captures.forEach { (key, value) ->
                                    put(key, value)
                                }
                            }
                            .toString()
                        )
            }
        else
            listOf()

        return promptFormat.formatPrompt(
            system = SYSTEM,
            examples = examples,
            actualQuery = userInput,
        )
    }

    /**
     * Once the LLM has generated output for a specific user input, after being prompted with
     * [formatLlmPrompt], this function takes care of actually evaluating the corresponding skill.
     */
    suspend fun processInput(
        userInput: String,
        llmOutput: String,
    ): RenderableItem {
        val (skillId, captures) = try {
            val data = JSONObject(llmOutput.trim())
            val skillId = data.getString("skill")
            data.remove("skill")
            val captures = data.keys().asSequence().map {
                it to data.getString(it)
            }.toMap()
            skillId to captures

        } catch (e: JSONException) {
            Log.e(TAG, "Llm provided invalid output: $llmOutput", e)
            return ErrorItem(e, llmOutput, false)
        }

        val skill = skills[skillId]
        return if (skill == null) {
            val error = Exception("Llm provided invalid skillId: $skillId")
            Log.e(TAG, "Llm provided invalid skillId \"$skillId\": $llmOutput", error)
            ErrorItem(error, llmOutput, false)

        } else {
            // we need to catch all exceptions thrown by the skill
            @Suppress("detekt:exceptions:TooGenericExceptionCaught")
            try {
                skill.computeOutput(userInput, captures, skillContext)
            } catch (t: Throwable) {
                Log.e(TAG, "Skill computeOutput failed", t)
                ErrorItem(t, llmOutput, false)
            }
        }
    }

    companion object {
        private val TAG = SkillHandler::class.simpleName
        private const val SYSTEM = "You are an AI assistant that should interpret the user " +
                "commands, understand which skill the user wants to use, extract relevant fields " +
                "to pass to the skill, and output the collected data in JSON format."
    }
}
