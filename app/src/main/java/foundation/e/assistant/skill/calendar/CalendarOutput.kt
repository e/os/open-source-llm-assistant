/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.calendar

import android.text.format.DateUtils
import android.text.format.DateUtils.FORMAT_SHOW_DATE
import android.text.format.DateUtils.FORMAT_SHOW_TIME
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import androidx.compose.ui.unit.dp
import foundation.e.assistant.R
import foundation.e.assistant.item.RenderableItem
import java.time.Duration
import java.time.Instant
import java.time.ZoneId

data class CalendarOutput(
    val title: String,
    val begin: Instant?,
    val end: Instant?,
    val allDay: Boolean,
) : RenderableItem {
    @Composable
    override fun Render() {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth(),
        ) {
            val trimmedTitle = title.trim()
            Text(
                text = if (trimmedTitle.isEmpty())
                    stringResource(R.string.calendar_set_event)
                else
                    stringResource(R.string.calendar_set_event_name, trimmedTitle),
                style = MaterialTheme.typography.h5,
                textAlign = TextAlign.Center,
            )

            val beginOrEnd = begin ?: end
            if (beginOrEnd != null) {
                Spacer(modifier = Modifier.height(4.dp))

                val flags = (if (allDay) 0 else FORMAT_SHOW_TIME) or
                        (if (sameDate(Instant.now(), begin, end)) 0 else FORMAT_SHOW_DATE)
                Text(
                    text = if (begin != null && end != null)
                        DateUtils.formatDateRange(
                            LocalContext.current,
                            begin.toEpochMilli(),
                            end.toEpochMilli(),
                            flags
                        )
                    else
                        DateUtils.formatDateTime(
                            LocalContext.current,
                            beginOrEnd.toEpochMilli(),
                            flags,
                        ),
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}

/**
 * @return whether all items in `others` are either null, or they refer to the same date as `first`
 */
private fun sameDate(first: Instant, vararg others: Instant?): Boolean {
    val firstDate = first.atZone(ZoneId.systemDefault()).toLocalDate()
    return others.all { it == null || it.atZone(ZoneId.systemDefault()).toLocalDate() == firstDate }
}

@Suppress("detekt:style:MagicNumber")
private class CalendarOutputParamProv : CollectionPreviewParameterProvider<CalendarOutput>(listOf(
    CalendarOutput("", null, null, false),
    CalendarOutput("Lorem ipsum dolor sit amet, consectetur", null, null, true),
    CalendarOutput("today", Instant.now(), null, false),
    CalendarOutput("tomorrow", Instant.now() + Duration.ofDays(1), null, false),
    CalendarOutput(" title ", Instant.now(), null, true),
    CalendarOutput("", Instant.now(), Instant.now() + Duration.ofDays(1), true),
    CalendarOutput("long title ".repeat(16), Instant.now(), Instant.now() + Duration.ofDays(1), false),
))

@Preview(showBackground = true)
@Composable
private fun CalendarOutputPreview(
    @PreviewParameter(CalendarOutputParamProv::class) calendarOutput: CalendarOutput
) {
    calendarOutput.Render()
}
