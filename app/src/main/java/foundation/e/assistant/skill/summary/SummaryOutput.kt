/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.summary

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.LocalContentColor
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import foundation.e.assistant.R
import foundation.e.assistant.item.RenderableItem

class SummaryOutput(
    private val userInput: String,
    private var summary: String?
) : RenderableItem {
    @Composable
    override fun Render() {
        val viewModel: SummaryViewModel = hiltViewModel(
            key = userInput,
            creationCallback = { factory: SummaryViewModel.Factory ->
                factory.create(userInput, summary)
            }
        )
        val isGenerating by viewModel.isGenerating.collectAsState()
        val viewModelSummary by viewModel.summary.collectAsState()

        if (!isGenerating) {
            // This is useless at the moment, but if in the future `RenderableItem`s will be stored
            // to disk and restored when the app is newly opened, we also need to keep track of the
            // (updated) state.
            summary = viewModelSummary
        }

        SummaryWithRegenerateButton(viewModelSummary, isGenerating, viewModel::regenerate)
    }
}

@Composable
private fun SummaryWithRegenerateButton(
    summary: String,
    isGenerating: Boolean,
    onRegenerateClick: () -> Unit,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        CompositionLocalProvider(
            LocalContentAlpha provides if (isGenerating) ContentAlpha.medium else ContentAlpha.high
        ) {
            if (summary.isBlank()) {
                CircularProgressIndicator(color = LocalContentColor.current)
            } else {
                Text(
                    text = summary.trim(),
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Justify,
                )
            }
        }

        TextButton(
            onClick = onRegenerateClick,
            enabled = !isGenerating,
            colors = ButtonDefaults.textButtonColors(
                contentColor = LocalContentColor.current
            ),
        ) {
            Text(
                text = stringResource(R.string.regenerate)
            )
        }
    }
}

@Preview
@Composable
private fun SummaryWithRegenerateButtonPreviewGenerating() {
    SummaryWithRegenerateButton(
        summary = "The passage depicts",
        isGenerating = true,
        onRegenerateClick = {},
    )
}

@Preview
@Composable
private fun SummaryWithRegenerateButtonPreviewFinal() {
    SummaryWithRegenerateButton(
        summary = "Officiis quia nobis molestias impedit. Ducimus nisi quod suscipit sed " +
                "reiciendis quas qui quibusdam. Quibusdam fugiat et accusamus. Tempore quisquam " +
                "maxime deserunt natus labore voluptatibus neque eligendi. A et expedita sit aut " +
                "sapiente et ipsum nobis.",
        isGenerating = false,
        onRegenerateClick = {},
    )
}
