/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap.models

import com.google.gson.annotations.SerializedName

data class WeatherDayResponse(
    val list: List<WeatherDay>,
)

data class WeatherDay(
    val temp: DayTemps,
    val weather: List<Weather>,
    @SerializedName("speed") val windSpeed: Double,
)

data class DayTemps(
    val day: Double,
    val min: Double,
    val max: Double,
)
