/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.summary

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import dagger.hilt.android.lifecycle.HiltViewModel
import foundation.e.assistant.llm.LlamacppManager
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@HiltViewModel(assistedFactory = SummaryViewModel.Factory::class)
class SummaryViewModel @AssistedInject constructor(
    @Assisted("userInput") private val userInput: String,
    @Assisted("initialSummary") initialSummary: String?,
    application: Application,
    private val llamacppManager: LlamacppManager,
) : AndroidViewModel(application) {

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("userInput") userInput: String,
            @Assisted("initialSummary") initialSummary: String?,
        ): SummaryViewModel
    }

    private val _summary = MutableStateFlow(initialSummary ?: "")
    val summary: StateFlow<String> = _summary
    private val _isGenerating = MutableStateFlow(false)
    val isGenerating: StateFlow<Boolean> = _isGenerating

    init {
        if (initialSummary.isNullOrEmpty()) {
            regenerate()
        }
    }

    fun regenerate() {
        viewModelScope.launch {
            if (_isGenerating.compareAndSet(expect = false, update = true)) {
                try {
                    llamacppManager.tryLoadFromSettingsSuspend()
                    val model = llamacppManager.getCurrentModel() ?: return@launch

                    // The SYSTEM prompt is enough to get good summaries, but leaving this code
                    // here in case it's needed in the future.
                    // examples = summarySkillData.suffled().map {
                    //     it.query to it.captures["summary"]
                    // }

                    val prompt = model.promptFormat.formatPrompt(
                        system = SYSTEM,
                        examples = listOf(),
                        actualQuery = userInput,
                    )

                    llamacppManager.getLlmResponse(
                        prompt = prompt,
                        maxNewTokens = MAX_NEW_TOKENS,
                        stopAtChar = null,
                        temperature = 0.7f, // some temperature for summaries is ok
                    ).collect {
                        _summary.value = it
                    }
                } finally {
                    _isGenerating.value = false
                }
            }
        }
    }

    companion object {
        const val SYSTEM = "You are an AI assistant whose task is to summarize text."
        const val MAX_NEW_TOKENS = 512
    }
}
