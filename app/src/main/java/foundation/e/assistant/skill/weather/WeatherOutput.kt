/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.weather

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import androidx.compose.ui.unit.dp
import foundation.e.assistant.R
import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.skill.weather.openweathermap.models.Coordinate
import foundation.e.assistant.skill.weather.openweathermap.models.DayTemps
import foundation.e.assistant.skill.weather.openweathermap.models.Main
import foundation.e.assistant.skill.weather.openweathermap.models.Sys
import foundation.e.assistant.skill.weather.openweathermap.models.Weather
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherData
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherDay
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherIcon
import foundation.e.assistant.skill.weather.openweathermap.models.Wind
import foundation.e.assistant.ui.AppTheme
import foundation.e.assistant.util.concatStringsForUi
import foundation.e.assistant.util.lowercaseCapitalized
import foundation.e.assistant.util.primaryLocale

data class WeatherOutput(
    val place: String,
    val current: WeatherData,
    val forecasts: List<WeatherDay>,
) : RenderableItem {
    @Composable
    override fun Render() {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            CurrentWeatherRow(place, current)

            for (forecast in forecasts) {
                ForecastRow(forecast)
            }
        }
    }
}

@Composable
fun CurrentWeatherRow(place: String, current: WeatherData) {
    val locale = LocalContext.current.primaryLocale
    val weather = current.weather[0]

    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        WeatherImage(weather = weather, widthFraction = 0.38f)

        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = place.lowercaseCapitalized(locale),
                style = MaterialTheme.typography.h4,
                textAlign = TextAlign.Center,
            )
            Text(
                text = concatStringsForUi(
                    weather.description.lowercaseCapitalized(locale),
                    current.main.temp?.let {
                        stringResource(
                            R.string.weather_temp_celsius,
                            it
                        )
                    },
                ),
                textAlign = TextAlign.Center,
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = concatStringsForUi(
                    current.main.tempMin?.let { stringResource(R.string.weather_min_celsius, it) },
                    current.main.tempMax?.let { stringResource(R.string.weather_max_celsius, it) },
                    current.wind.speed?.let {
                        stringResource(
                            R.string.weather_wind_m_s,
                            it
                        )
                    },
                ),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.caption,
            )
        }
    }
}

@Composable
fun ForecastRow(forecast: WeatherDay) {
    val weather = forecast.weather[0]

    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        WeatherImage(weather = weather, widthFraction = 0.14f)
        Spacer(modifier = Modifier.width(4.dp))

        Column(
            modifier = Modifier.fillMaxWidth(),
        ) {
            Text(
                text = concatStringsForUi(
                    weather.description.lowercaseCapitalized(LocalContext.current.primaryLocale),
                    stringResource(R.string.weather_temp_celsius, forecast.temp.day),
                ),
            )
            Text(
                text = concatStringsForUi(
                    stringResource(R.string.weather_min_celsius, forecast.temp.min),
                    stringResource(R.string.weather_max_celsius, forecast.temp.max),
                    stringResource(R.string.weather_wind_m_s, forecast.windSpeed),
                ),
                style = MaterialTheme.typography.caption,
            )
        }
    }
}

@Composable
fun WeatherImage(weather: Weather, widthFraction: Float) {
    Image(
        painter = painterResource(WeatherIcon.fromCode(weather.id).icon),
        contentDescription = weather.description,
        modifier = Modifier
            .fillMaxWidth(widthFraction)
            .aspectRatio(1.0f),
    )
}

@Suppress("detekt:style:MagicNumber", "detekt:style:MaxLineLength")
private class WeatherOutputPreviews : CollectionPreviewParameterProvider<WeatherOutput>(listOf(
    WeatherOutput(
        place = "Rome",
        current = WeatherData(
            code = 0,
            id = 0,
            name = "",
            main = Main(
                temp = 4.65,
                feelsLike = null,
                tempMin = -0.512,
                tempMax = 7.36,
                pressure = null,
                humidity = null,
            ),
            sys = Sys(
                type = null,
                id = null,
                country = null,
                sunrise = null,
                sunset = null,
            ),
            dt = 0,
            coord = Coordinate(0.0, 0.0),
            visibility = 0,
            timezone = 0,
            weather = listOf(Weather(
                id = 0,
                main = "Clear",
                description = "clear sky",
                icon = "01n",
            )),
            wind = Wind(
                speed = 3.0,
                deg = null,
            ),
            clouds = null,
            rain = null,
            snow = null,
        ),
        forecasts = listOf(
            WeatherDay(
                temp = DayTemps(
                    day = 3.55,
                    min = 2.1,
                    max = 6.0,
                ),
                weather = listOf(Weather(
                    id = 601,
                    main = "",
                    description = "",
                    icon = "",
                )),
                windSpeed = 15.7,
            ),
            WeatherDay(
                temp = DayTemps(
                    day = 7.0,
                    min = 1.41,
                    max = 9.38,
                ),
                weather = listOf(Weather(
                    id = 703,
                    main = "",
                    description = "Mist",
                    icon = "",
                )),
                windSpeed = 3.5,
            ),
            WeatherDay(
                temp = DayTemps(
                    day = 40.0,
                    min = 38.6,
                    max = 41.1,
                ),
                weather = listOf(Weather(
                    id = 212,
                    main = "",
                    description = "It's now really really hot, and there's lots of wind, too, lorem ipsum",
                    icon = "",
                )),
                windSpeed = 500.0,
            ),
        )
    ),
    WeatherOutput(
        place = "Gasselterboerveenschemond",
        current = WeatherData(
            code = 0,
            id = 0,
            name = "",
            main = Main(
                temp = 48.65,
                feelsLike = null,
                tempMin = -48.91,
                tempMax = 52.11,
                pressure = null,
                humidity = null,
            ),
            sys = Sys(
                type = null,
                id = null,
                country = null,
                sunrise = null,
                sunset = null,
            ),
            dt = 0,
            coord = Coordinate(0.0, 0.0),
            visibility = 0,
            timezone = 0,
            weather = listOf(Weather(
                id = 500,
                main = "Clear",
                description = "really long description lorem ipsum dolor sit amet",
                icon = "01n",
            )),
            wind = Wind(
                speed = 375.0,
                deg = null,
            ),
            clouds = null,
            rain = null,
            snow = null,
        ),
        forecasts = listOf(),
    )
))

@Preview
@Composable
private fun WeatherOutputPreview(
    @PreviewParameter(WeatherOutputPreviews::class) data: WeatherOutput
) {
    AppTheme {
        Surface(color = MaterialTheme.colors.background) {
            data.Render()
        }
    }
}
