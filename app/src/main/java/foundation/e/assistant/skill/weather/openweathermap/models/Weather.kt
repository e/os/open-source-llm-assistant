/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap.models

import com.google.gson.annotations.SerializedName

data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String,
)

data class Main(
    val temp: Double? = null,
    @SerializedName(value = "feels_like") val feelsLike: Double? = null,
    @SerializedName(value = "temp_min") val tempMin: Double? = null,
    @SerializedName(value = "temp_max") val tempMax: Double? = null,
    val pressure: Double? = null,
    val humidity: Double? = null,
)

data class Wind(
    val speed: Double? = null,
    val deg: Int? = null,
)

data class Clouds(
    val all: Int? = null,
)

data class Rain(
    val oneHour: Double? = null,
    val threeHours: Double? = null,
)

data class Snow(
    val oneHour: Double? = null,
    val threeHours: Double? = null,
)

data class Sys(
    val type: Int? = null,
    val id: Int? = null,
    val country: String? = null,
    val sunrise: Long? = null,
    val sunset: Long? = null,
)

data class CombinedWeatherResponse(
    val current: WeatherData? = null,
    val forecasts: WeatherDayResponse? = null,
)
