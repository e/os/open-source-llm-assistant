/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap.api

import foundation.e.assistant.skill.weather.openweathermap.models.Coordinate
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherCity
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherData
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherDayResponse
import foundation.e.assistant.util.ApiResult

interface OpenWeatherRepository {
    suspend fun getWeatherByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherData>

    suspend fun getDaysForecastByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherDayResponse>

    suspend fun getWeatherByLocationName(
        name: String,
    ): ApiResult<WeatherData>

    suspend fun getLocationCoordsByName(
        name: String,
        limit: Int = 1,
    ): ApiResult<List<WeatherCity>>

    suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int = 1,
    ): ApiResult<List<WeatherCity>>
}
