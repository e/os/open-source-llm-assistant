/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.calendar

import android.content.Intent
import android.provider.CalendarContract
import android.util.Log
import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.skill.Skill
import foundation.e.assistant.skill.SkillContext
import foundation.e.assistant.skill_data.calendar
import java.time.ZoneOffset
import java.time.ZonedDateTime

class CalendarSkill : Skill {
    override val llmData = calendar.llmData

    override suspend fun computeOutput(
        userInput: String,
        captures: Map<String, String>,
        ctx: SkillContext
    ): RenderableItem {
        // TODO allow the user to say begin and end time
        // TODO make events to be all-day if the user specifies no hour
        val begin = captures[calendar.`when`]
            ?.let { ctx.parserFormatter.extractDateTime(it).first }
            ?.let { ZonedDateTime.of(it, ZoneOffset.systemDefault()) }
            ?.toInstant()
        val title = captures[calendar.name] ?: ""

        Log.i(TAG, "Setting an event named \"$title\" for $begin: $captures")
        val intent = Intent(Intent.ACTION_EDIT).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            data = CalendarContract.Events.CONTENT_URI
            putExtra(CalendarContract.Events.TITLE, title)
            if (begin != null) {
                putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, begin.toEpochMilli())
                putExtra(CalendarContract.EXTRA_EVENT_END_TIME, begin.toEpochMilli())
            }
            putExtra(CalendarContract.Events.ALL_DAY, false)
        }
        ctx.appContext.startActivity(intent)

        return CalendarOutput(
            title = title,
            begin = begin,
            end = null,
            allDay = false,
        )
    }

    companion object {
        val TAG = CalendarSkill::class.simpleName;
    }
}
