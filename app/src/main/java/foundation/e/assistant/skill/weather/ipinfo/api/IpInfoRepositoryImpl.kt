/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.weather.ipinfo.api

import foundation.e.assistant.skill.weather.ipinfo.models.IpInfoCoordinate
import foundation.e.assistant.util.ApiError
import foundation.e.assistant.util.ApiException
import foundation.e.assistant.util.ApiResult
import foundation.e.assistant.util.ApiSuccess
import foundation.e.assistant.util.handleApi
import javax.inject.Inject


class IpInfoRepositoryImpl @Inject constructor(
    private val baseApi: IpInfoApi,
) : IpInfoRepository {
    override suspend fun getIpInfo(): ApiResult<IpInfoCoordinate> = handleApi {
        baseApi.getIpInfo()
    }.let {
        when (it) {
            is ApiError -> ApiError(it.code, it.message)
            is ApiException -> ApiException(it.e)
            is ApiSuccess -> {
                val latLon = it.data.loc.split(',', limit = 2)
                ApiSuccess(
                    IpInfoCoordinate(
                        city = it.data.city,
                        lat = latLon.getOrNull(0)?.toDoubleOrNull() ?: return ApiException(
                            Exception("Cannot parse latitude from ${it.data.loc}")),
                        lon = latLon.getOrNull(1)?.toDoubleOrNull() ?: return ApiException(
                            Exception("Cannot parse longitude from ${it.data.loc}")),
                    )
                )
            }
        }
    }
}
