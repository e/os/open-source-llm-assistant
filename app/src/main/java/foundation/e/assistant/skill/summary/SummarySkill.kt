/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.summary

import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.skill.Skill
import foundation.e.assistant.skill.SkillContext
import foundation.e.assistant.skill_data.summary

class SummarySkill : Skill {
    override val llmData = summary.llmData

    override suspend fun computeOutput(
        userInput: String,
        captures: Map<String, String>,
        ctx: SkillContext
    ): RenderableItem {
        // in order to do summary in one pass, use the commented-out examples in summary.json
        return SummaryOutput(
            userInput = userInput,
            summary = captures[summary.summary],
        )
    }
}
