/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.open

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import foundation.e.assistant.item.RenderableItem
import foundation.e.assistant.skill.Skill
import foundation.e.assistant.skill.SkillContext
import foundation.e.assistant.skill_data.open
import foundation.e.assistant.util.StringDistanceUtil

/**
 * Partially taken from Dicio:
 * https://github.com/Stypox/dicio-android/blob/c99e375dac6406dc94922583b680c97fe968bf17/app/src/main/java/org/stypox/dicio/skills/open/OpenOutput.kt
 */
class OpenSkill : Skill {
    override val llmData = open.llmData

    override suspend fun computeOutput(
        userInput: String,
        captures: Map<String, String>,
        ctx: SkillContext
    ): RenderableItem {
        val packageManager: PackageManager = ctx.appContext.packageManager
        val applicationInfo = captures[open.app]
            ?.trim()
            ?.let { getMostSimilarApp(packageManager, it) }

        if (applicationInfo != null) {
            val launchIntent: Intent =
                packageManager.getLaunchIntentForPackage(applicationInfo.packageName)!!
            launchIntent.action = Intent.ACTION_MAIN
            launchIntent.addCategory(Intent.CATEGORY_LAUNCHER)
            launchIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            ctx.appContext.startActivity(launchIntent)
        }

        return OpenOutput(
            appName = applicationInfo?.loadLabel(packageManager)?.toString() ?: captures["app"],
            packageName = applicationInfo?.packageName,
        )
    }

    companion object {
        /**
         * Taken from Dicio (GPLv3 license):
         * https://github.com/Stypox/dicio-android/blob/c99e375dac6406dc94922583b680c97fe968bf17/app/src/main/java/org/stypox/dicio/util/StringUtils.kt
         */
        private fun getMostSimilarApp(
            packageManager: PackageManager,
            appName: String
        ): ApplicationInfo? {
            val resolveInfosIntent = Intent(Intent.ACTION_MAIN, null)
            resolveInfosIntent.addCategory(Intent.CATEGORY_LAUNCHER)

            @SuppressLint("QueryPermissionsNeeded") // we need to query all apps
            val resolveInfos: List<ResolveInfo> =
                packageManager.queryIntentActivities(resolveInfosIntent, 0)
            var bestDistance = Int.MAX_VALUE
            var bestApplicationInfo: ApplicationInfo? = null

            for (resolveInfo in resolveInfos) {
                try {
                    val currentApplicationInfo: ApplicationInfo = packageManager.getApplicationInfo(
                        resolveInfo.activityInfo.packageName, PackageManager.GET_META_DATA
                    )
                    val currentDistance = StringDistanceUtil.customStringDistance(
                        appName,
                        packageManager.getApplicationLabel(currentApplicationInfo).toString()
                    )
                    if (currentDistance < bestDistance) {
                        bestDistance = currentDistance
                        bestApplicationInfo = currentApplicationInfo
                    }
                } catch (ignored: PackageManager.NameNotFoundException) {
                }
            }
            return if (bestDistance > MAX_ACCEPTABLE_DISTANCE) null else bestApplicationInfo
        }

        private const val MAX_ACCEPTABLE_DISTANCE = 5
    }
}
