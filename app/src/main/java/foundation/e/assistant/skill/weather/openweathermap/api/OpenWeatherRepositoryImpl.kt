/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap.api

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import foundation.e.assistant.BuildConfig
import foundation.e.assistant.di.IMPERIAL_UNITS
import foundation.e.assistant.di.OPEN_WEATHER_MAP_API_KEY
import foundation.e.assistant.skill.weather.openweathermap.models.Coordinate
import foundation.e.assistant.skill.weather.openweathermap.models.Units
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherCity
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherData
import foundation.e.assistant.skill.weather.openweathermap.models.WeatherDayResponse
import foundation.e.assistant.util.ApiResult
import foundation.e.assistant.util.handleApi
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class OpenWeatherRepositoryImpl @Inject constructor(
    private val baseApi: OpenWeatherApi.Base,
    private val geoApi: OpenWeatherApi.Geo,
    private val prefs: DataStore<Preferences>,
) : OpenWeatherRepository {

    private suspend fun getApiKey() =
        prefs.data.first()[OPEN_WEATHER_MAP_API_KEY] ?: BuildConfig.OPEN_WEATHER_MAP_API_KEY

    private suspend fun getUnit() = if (prefs.data.first()[IMPERIAL_UNITS] == true) {
        Units.IMPERIAL
    } else {
        Units.METRIC
    }

    override suspend fun getWeatherByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherData> = handleApi {
        baseApi.getWeatherByCoords(coord.lat, coord.lon, getApiKey(), getUnit())
    }

    override suspend fun getDaysForecastByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherDayResponse> = handleApi {
        baseApi.getForecastByCoords(coord.lat, coord.lon, getApiKey(), getUnit())
    }

    override suspend fun getWeatherByLocationName(
        name: String,
    ): ApiResult<WeatherData> = handleApi {
        baseApi.getWeatherByLocationName(name, getApiKey(), getUnit())
    }

    override suspend fun getLocationCoordsByName(
        name: String,
        limit: Int,
    ): ApiResult<List<WeatherCity>> = handleApi {
        geoApi.getLocationCoordsByName(name, limit, getApiKey(), getUnit())
    }

    override suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int,
    ): ApiResult<List<WeatherCity>> = handleApi {
        geoApi.getLocationNameByCoords(coord.lat, coord.lon, limit, getApiKey(), getUnit())
    }
}
