/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import foundation.e.assistant.util.primaryLocale
import okhttp3.OkHttpClient
import org.dicio.numbers.ParserFormatter
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Can be used to access various resources, including the Android context.
 */
@Singleton
data class SkillContext @Inject constructor(
    @ApplicationContext val appContext: Context,
    val httpClient: OkHttpClient,
) {
    // the locale was set dynamically by LocaleManager on app startup
    val locale: Locale = appContext.primaryLocale

    val parserFormatter = ParserFormatter(locale)
}
