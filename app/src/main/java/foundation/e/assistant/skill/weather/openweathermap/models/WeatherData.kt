/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap.models

import com.google.gson.annotations.SerializedName

data class WeatherData(
    @SerializedName("cod") val code: Int,
    val id: Int,
    val name: String,
    val main: Main,
    val sys: Sys,
    val dt: Long,
    val coord: Coordinate,
    val visibility: Int,
    val timezone: Int,
    val weather: List<Weather>,
    val wind: Wind,
    val clouds: Clouds? = null,
    val rain: Rain? = null,
    val snow: Snow? = null,
)
