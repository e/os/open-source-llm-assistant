/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.skill.open

import android.content.pm.PackageManager.NameNotFoundException
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import androidx.compose.ui.unit.dp
import com.google.accompanist.drawablepainter.rememberDrawablePainter
import foundation.e.assistant.R
import foundation.e.assistant.item.RenderableItem

data class OpenOutput(
    val appName: String?,
    val packageName: String?,
) : RenderableItem {
    @Composable
    override fun Render() {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            if (packageName != null) {
                val context = LocalContext.current
                val icon = remember {
                    try {
                        context.packageManager.getApplicationIcon(packageName)
                    } catch (e: NameNotFoundException) {
                        Log.e(TAG, "Could not load icon for $packageName", e)
                        null
                    }
                }

                if (icon != null) {
                    Image(
                        painter = rememberDrawablePainter(icon),
                        contentDescription = appName,
                        modifier = Modifier
                            .fillMaxWidth(0.2f)
                            .aspectRatio(1.0f),
                    )

                    Spacer(modifier = Modifier.width(8.dp))
                }
            }

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxWidth(),
            ) {
                Text(
                    text = if (appName == null)
                        stringResource(R.string.open_could_not_understand)
                    else if (packageName == null)
                        stringResource(R.string.open_app_not_found, appName)
                    else
                        stringResource(R.string.open_opened, appName),
                    style = MaterialTheme.typography.h5,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth(),
                )

                if (packageName != null) {
                    Text(
                        text = packageName,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth(),
                    )
                }
            }
        }
    }

    companion object {
        val TAG = OpenOutput::class.simpleName
    }
}

@Suppress("detekt:style:MagicNumber")
private class OpenOutputParamProv : CollectionPreviewParameterProvider<OpenOutput>(listOf(
    OpenOutput(null, null),
    OpenOutput("Gallery", null),
    OpenOutput(null, "org.schabi.newpipe"), // impossible situation, preview on pc won't work
    OpenOutput("Calendar", "org.fossify.calendar"), // preview on pc won't work
))

@Preview(showBackground = true)
@Composable
private fun OpenOutputPreview(
    @PreviewParameter(OpenOutputParamProv::class) openOutput: OpenOutput
) {
    openOutput.Render()
}
