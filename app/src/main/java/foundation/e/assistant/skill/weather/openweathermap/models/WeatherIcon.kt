/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap.models

import androidx.annotation.DrawableRes
import foundation.e.assistant.R

enum class WeatherIcon(@DrawableRes val icon: Int) {
    CLEAR_DAY(R.drawable.weather_color_32),
    CLEAR_NIGHT(R.drawable.weather_color_31),
    CLOUDS(R.drawable.weather_color_26),
    FEW_CLOUDS_DAY(R.drawable.weather_color_28),
    FEW_CLOUDS_NIGHT(R.drawable.weather_color_27),
    RAIN(R.drawable.weather_color_40),
    SHOWER_RAIN(R.drawable.weather_color_12),
    FREEZING_RAIN(R.drawable.weather_color_35),
    SNOW(R.drawable.weather_color_43),
    THUNDERSTORM(R.drawable.weather_color_3),
    MIST(R.drawable.weather_color_22),
    DRIZZLE(R.drawable.weather_color_11);

    companion object {
        @Suppress("detekt:style:MagicNumber")
        fun fromCode(code: Int) =
            when (code) {
                in 200..232 -> THUNDERSTORM
                in 300..321 -> DRIZZLE
                in 500..504 -> RAIN
                511 -> FREEZING_RAIN
                in 520..531 -> SHOWER_RAIN
                in 600..622 -> SNOW
                in 701..781 -> MIST
                800 -> CLEAR_DAY
                801 -> FEW_CLOUDS_DAY
                in 802..804 -> CLOUDS
                else -> CLEAR_DAY
            }
    }
}
