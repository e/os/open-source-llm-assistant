/*
 * Copyright © e Foundation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.assistant.skill.weather.openweathermap

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import foundation.e.assistant.skill.weather.openweathermap.api.OpenWeatherApi
import foundation.e.assistant.skill.weather.openweathermap.api.OpenWeatherRepository
import foundation.e.assistant.skill.weather.openweathermap.api.OpenWeatherRepositoryImpl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier @Retention(AnnotationRetention.BINARY) annotation class OwmBaseRetrofit

@Qualifier @Retention(AnnotationRetention.BINARY) annotation class OwmGeoRetrofit

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Singleton
    @Provides
    @OwmBaseRetrofit
    fun provideBaseRetrofit(client: OkHttpClient, gson: Gson): Retrofit =
        Retrofit.Builder()
            .baseUrl(OpenWeatherApi.BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()

    @Singleton
    @Provides
    @OwmGeoRetrofit
    fun provideGeoRetrofit(client: OkHttpClient, gson: Gson): Retrofit =
        Retrofit.Builder()
            .baseUrl(OpenWeatherApi.BASE_GEOCODING_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()

    @Singleton
    @Provides
    fun provideBaseApi(@OwmBaseRetrofit retrofit: Retrofit): OpenWeatherApi.Base =
        retrofit.create(OpenWeatherApi.Base::class.java)

    @Singleton
    @Provides
    fun provideGeoApi(@OwmGeoRetrofit retrofit: Retrofit): OpenWeatherApi.Geo =
        retrofit.create(OpenWeatherApi.Geo::class.java)

    @Singleton
    @Provides
    fun provideRepository(
        baseApi: OpenWeatherApi.Base,
        geoApi: OpenWeatherApi.Geo,
        prefs: DataStore<Preferences>,
    ): OpenWeatherRepository = OpenWeatherRepositoryImpl(baseApi, geoApi, prefs)
}
