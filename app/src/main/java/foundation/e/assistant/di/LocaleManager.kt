/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.di

import android.content.Context
import android.util.Log
import androidx.core.os.ConfigurationCompat
import dagger.hilt.android.qualifiers.ApplicationContext
import foundation.e.assistant.skill_data.languages
import foundation.e.assistant.util.UnsupportedLocaleException
import foundation.e.assistant.util.primaryLocale
import foundation.e.assistant.util.resolveSupportedLocale
import java.util.Locale
import javax.inject.Inject

/**
 * Chooses locale and localeString from languages configured in the user's system, making sure in
 * particular that skill examples exist for the chosen locale, because otherwise the LLM wouldn't
 * work.
 */
class LocaleManager @Inject constructor(
    @ApplicationContext appContext: Context
) {
    var locale: Locale private set
    var localeString: String private set

    // this init block is executed at the start of the app, because LocaleManager is injected by
    // Hilt in MainActivity
    init {
        // TODO maybe let the user choose the language in settings?
        try {
            val resolutionResult = resolveSupportedLocale(
                ConfigurationCompat.getLocales(appContext.resources.configuration),
                languages
            )
            locale = resolutionResult.availableLocale
            localeString = resolutionResult.supportedLocaleString
        } catch (e: UnsupportedLocaleException) {
            Log.e(TAG, "Can't select locale, using english", e)
            locale = Locale(
                "en",
                appContext.primaryLocale.country,
                appContext.primaryLocale.variant
            )
            localeString = "en"
        }
    }

    companion object {
        val TAG = LocaleManager::class.simpleName
    }
}
