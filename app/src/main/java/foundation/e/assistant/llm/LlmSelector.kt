/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.llm

import android.content.res.Resources
import android.icu.number.Notation
import android.icu.number.NumberFormatter
import android.icu.text.CompactDecimalFormat
import android.os.Build
import android.text.format.Formatter
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.DropdownMenu
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Archive
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material.icons.filled.Download
import androidx.compose.material.icons.filled.DownloadDone
import androidx.compose.material.icons.filled.Error
import androidx.compose.material.icons.filled.UploadFile
import androidx.compose.material.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import foundation.e.assistant.R
import foundation.e.assistant.llm.LlmModelState.Downloading
import foundation.e.assistant.llm.LlmModelState.ErrorDownloading
import foundation.e.assistant.llm.LlmModelState.ErrorLoading
import foundation.e.assistant.llm.LlmModelState.Loaded
import foundation.e.assistant.llm.LlmModelState.Loading
import foundation.e.assistant.llm.LlmModelState.NotDownloaded
import foundation.e.assistant.llm.LlmModelState.NotLoaded
import foundation.e.assistant.llm.model.LlmModel
import foundation.e.assistant.util.LoadingProgress
import foundation.e.assistant.util.SmallCircularProgressIndicator
import foundation.e.assistant.util.loadingProgressString
import kotlin.math.ln
import kotlin.math.pow

@Composable
fun LlmSelector() {
    val llmSelectorViewModel: LlmSelectorViewModel = viewModel()
    val modelStates = llmSelectorViewModel.modelListStates.collectAsState().value
    val busyLoading = llmSelectorViewModel.busyLoading.collectAsState().value
    val selectedModel = llmSelectorViewModel.selectedModel.collectAsState().value

    LlmSelector(
        modelStates = modelStates,
        busyLoading = busyLoading,
        selectedModel = selectedModel,
        onSelectModel = llmSelectorViewModel::onSelectModel,
        onModelClick = llmSelectorViewModel::onModelClick,
    )
}

@Composable
fun LlmSelector(
    modelStates: List<Pair<LlmModel, LlmModelState>>,
    busyLoading: Boolean,
    selectedModel: LlmModel?,
    onSelectModel: (LlmModel) -> Unit,
    onModelClick: (model: LlmModel) -> Unit,
) {
    var expanded by rememberSaveable { mutableStateOf(false) }
    val (curModel, curState) = modelStates.firstOrNull { (model, _) -> model == selectedModel }
        ?: modelStates.firstOrNull()
        ?: Pair(null, NotDownloaded)

    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
    ) {
        Box(
            modifier = Modifier
                .clickable { expanded = !expanded }
                .fillMaxWidth()
                .weight(1f),
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth(),
            ) {
                if (curModel != null) {
                    LlmModelItem(
                        model = curModel,
                        state = curState,
                    )
                }

                Icon(
                    if (expanded) Icons.Default.ArrowDropUp else Icons.Default.ArrowDropDown,
                    contentDescription = stringResource(R.string.select_model),
                    modifier = Modifier.padding(horizontal = 8.dp)
                )
            }

            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier.fillMaxWidth(),
            ) {
                for ((model, state) in modelStates) {
                    LlmModelItemWithIcon(
                        model = model,
                        state = state,
                        modifier = Modifier
                            .clickable {
                                onSelectModel(model)
                                expanded = false
                            }
                            .fillMaxWidth(),
                    )
                }
            }
        }

        @Suppress("detekt:complexity:ComplexCondition")
        if (curModel == null || curState is Downloading || curState == Loading || curState == Loaded
            || (curState == NotLoaded && busyLoading)) {
            // disallow pressing the button if the next action would be to load a new model, but the
            // llm library is currently busy loading another model
            Box(
                modifier = Modifier
                    .padding(horizontal = 4.dp)
                    .minimumInteractiveComponentSize(),
                contentAlignment = Alignment.Center,
            ) {
                LlmModelButtonIcon(curState)
            }
        } else {
            IconButton(
                onClick = { onModelClick(curModel) },
                modifier = Modifier.padding(horizontal = 4.dp),
            ) {
                LlmModelButtonIcon(curState)
            }
        }
    }
}

@Composable
fun LlmModelItemWithIcon(model: LlmModel, state: LlmModelState, modifier: Modifier = Modifier) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier.padding(end = 16.dp),
    ) {
        LlmModelItem(model, state)
        LlmModelItemIcon(state)
    }
}

@Composable
fun LlmModelItem(model: LlmModel, state: LlmModelState) {
    Column(
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
    ) {
        Text(text = modelItemTitle(model))
        Spacer(modifier = Modifier.height(2.dp))

        Text(
            text = when (state) {
                is Downloading -> loadingProgressString(
                    LocalContext.current,
                    state.currentBytes,
                    state.totalBytes,
                )
                is ErrorDownloading -> stringResource(R.string.error_downloading)
                is ErrorLoading -> stringResource(R.string.error_loading)
                else -> Formatter.formatFileSize(LocalContext.current, model.fileSizeBytes)
            },
            style = MaterialTheme.typography.caption,
        )
    }
}

@Composable
fun LlmModelItemIcon(state: LlmModelState) {
    when (state) {
        NotDownloaded -> {}
        is Downloading -> LoadingProgress(state.currentBytes, state.totalBytes)
        is ErrorDownloading -> Icon(Icons.Default.Error, stringResource(R.string.error_downloading))
        NotLoaded -> Icon(Icons.Default.Archive, stringResource(R.string.downloaded))
        Loading -> SmallCircularProgressIndicator()
        is ErrorLoading -> Icon(Icons.Default.Error, stringResource(R.string.error_loading))
        Loaded -> Icon(Icons.Default.DownloadDone, stringResource(R.string.loaded))
    }
}

@Composable
fun LlmModelButtonIcon(state: LlmModelState) {
    when (state) {
        NotDownloaded -> Icon(Icons.Default.Download, stringResource(R.string.download))
        is Downloading -> LoadingProgress(state.currentBytes, state.totalBytes)
        is ErrorDownloading -> Icon(Icons.Default.Error, stringResource(R.string.error_downloading))
        NotLoaded -> Icon(Icons.Default.UploadFile, stringResource(R.string.downloaded))
        Loading -> SmallCircularProgressIndicator()
        is ErrorLoading -> Icon(Icons.Default.Error, stringResource(R.string.error_loading))
        Loaded -> Icon(Icons.Default.DownloadDone, stringResource(R.string.loaded))
    }
}

private fun modelItemTitle(model: LlmModel): String {
    var text = model.name
    if (model.parameters > 0) {
        text += " "
        text += shortNotation(model.parameters)
    }
    if (model.quantization.isNotEmpty()) {
        text += " "
        text += model.quantization
    }
    return text
}


private const val SHORT_NOTATION_BASE = 1_000f
private const val SHORT_NOTATION_BASE_LOG10 = 3
private const val SHORT_NOTATION_SUFFIXES = "kMGTPE"

/**
 * Converts the provided number into a string in short notation (e.g. 5M for 5000000), trying to use
 * Android standard functions if supported by the current Android version.
 *
 * Taken from https://stackoverflow.com/a/76352975.
 */
private fun shortNotation(num: Long): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val locale = Resources.getSystem().configuration.locales.get(0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) NumberFormatter.with()
            .notation(Notation.compactShort())
            .locale(locale)
            .format(num)
            .toString()
        else CompactDecimalFormat
            .getInstance(locale, CompactDecimalFormat.CompactStyle.SHORT)
            .format(num)
    } else {
        if (num < SHORT_NOTATION_BASE) return "$num"
        val ratio = ln(num.toDouble()) / ln(SHORT_NOTATION_BASE)
        val exp = ratio.toInt()
        val format = if ((ratio % 1) * SHORT_NOTATION_BASE_LOG10 < 1) "%.1f%c" else "%.0f%c"
        String.format(format, num / SHORT_NOTATION_BASE.pow(exp), SHORT_NOTATION_SUFFIXES[exp - 1])
    }
}
