/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.llm

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import dagger.hilt.android.qualifiers.ApplicationContext
import foundation.e.assistant.di.LAST_LOADED_LLAMACPP_MODEL
import foundation.e.assistant.llm.LlmModelState.Downloading
import foundation.e.assistant.llm.LlmModelState.ErrorDownloading
import foundation.e.assistant.llm.LlmModelState.ErrorLoading
import foundation.e.assistant.llm.LlmModelState.Loaded
import foundation.e.assistant.llm.LlmModelState.Loading
import foundation.e.assistant.llm.LlmModelState.NotDownloaded
import foundation.e.assistant.llm.LlmModelState.NotLoaded
import foundation.e.assistant.llm.model.LLM_MODELS
import foundation.e.assistant.llm.model.LlmModel
import foundation.e.assistant.util.downloadBinaryFileWithPartial
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Handles interactions with the lower level [Llamacpp] and with the even lower-level C++ native
 * code for llama.cpp. The [Llamacpp] and the native layers were taken from llama.cpp's Android
 * example. This class also handles downloading and keeping track of loaded/downloaded models.
 */
@Singleton
class LlamacppManager @Inject constructor(
    @ApplicationContext appContext: Context,
    private val okHttpClient: OkHttpClient,
    private val prefs: DataStore<Preferences>,
) {
    // if the map will become mutable, then every time it changes, the job that collects from
    // each model state flow to build the combined _uiState should be relaunched
    private val _modelStates: Map<LlmModel, MutableStateFlow<LlmModelState>>
    private val _modelListStates: MutableStateFlow<List<Pair<LlmModel, LlmModelState>>>
    val modelListStates: StateFlow<List<Pair<LlmModel, LlmModelState>>>
    private val _busyLoading = MutableStateFlow(false)
    val busyLoading: StateFlow<Boolean> = _busyLoading

    private val scope = CoroutineScope(Dispatchers.Default)

    private val filesDir: File = appContext.filesDir
    private val cacheDir: File = appContext.cacheDir
    private val llamacpp = Llamacpp.instance()
    private var curLoadedModel: LlmModel? = null
    private val triedToLoadFromSettings = AtomicBoolean(false)

    init {
        _modelStates = LLM_MODELS.associateWith {
            MutableStateFlow(if (File(filesDir, it.filename).isFile) NotLoaded else NotDownloaded)
        }
        _modelListStates = MutableStateFlow(
            _modelStates.map { (model, flow) -> model to flow.value }.toList()
        )
        modelListStates = _modelListStates

        scope.launch {
            // keep updating the modelListStates automatically based on each model's flow
            _modelStates.map { (model, flow) -> flow.map { data -> model to data } }
                .combineToList()
                .collect { modelListStates.value = it }
        }
    }


    /**
     * Loads the last model used by the user.
     */
    fun tryLoadFromSettings() {
        scope.launch {
            tryLoadFromSettingsSuspend()
        }
    }

    /**
     * Loads the last model used by the user.
     */
    suspend fun tryLoadFromSettingsSuspend() {
        // skip loading from settings and return, if any of the reasons in the comments are true

        if (triedToLoadFromSettings.getAndSet(true) || curLoadedModel != null) {
            // - we already tried to load from settings before (works atomically)
            // - there is already a loaded model
            return
        }

        val lastLoadedModelFilename = prefs.data.first()[LAST_LOADED_LLAMACPP_MODEL]
        val lastLoadedModel = _modelStates.keys.find { it.filename == lastLoadedModelFilename }
        if (lastLoadedModel == null || _modelStates[lastLoadedModel]?.value != NotLoaded ||
            !_busyLoading.compareAndSet(expect = false, update = true)) {
            // - the last model saved in settings is not recognized
            // - the last model saved in settings has not been downloaded yet
            // - a model is already being loaded
            return
        }

        loadSuspend(lastLoadedModel)
    }

    /**
     * If the model is not being downloaded/loaded, or if there was an error before,
     * downloads/loads the model.
     */
    fun advanceToNextState(model: LlmModel) {
        when (_modelStates[model]?.value) {
            NotDownloaded -> download(model)
            is Downloading -> {} // wait for download to finish
            is ErrorDownloading -> download(model) // retry
            NotLoaded, is ErrorLoading -> if (
                _busyLoading.compareAndSet(expect = false, update = true)
            ) {
                // only load the model if there is not already another model being loaded
                load(model)
            }
            Loading -> {} // wait for loading to finish
            Loaded -> {} // nothing to do
            null -> Log.e(TAG, "Model not found in model states: $model")
        }
    }

    /**
     * Opens a network connection to download a model. The [LlmModel.filename] will be
     * created/overwritten only if the download completes correctly (a temp file is used for partial
     * downloads). This ensures that just by checking whether [LlmModel.filename] exists, we can
     * see if the model was ever downloaded correctly or not.
     */
    private fun download(model: LlmModel) {
        val state = _modelStates[model]!!
        state.value = Downloading(0, 0)

        scope.launch(Dispatchers.IO) {
            try {
                val request: Request = Request.Builder().url(model.downloadUrl).build()
                val response = okHttpClient.newCall(request).execute()

                downloadBinaryFileWithPartial(
                    response = response,
                    file = File(filesDir, model.filename),
                    cacheDir = cacheDir,
                ) { currentBytes, totalBytes ->
                    state.value = Downloading(currentBytes, totalBytes)
                }
            } catch (e: IOException) {
                Log.e(TAG, "Can't download model ${model.name}", e)
                state.value = ErrorDownloading(e)
                return@launch
            }

            state.value = NotLoaded
        }
    }

    /**
     * Unloads any model and then loads the provided model into RAM.
     */
    private fun load(model: LlmModel) {
        // assumes that _llmBusyLoading=true is already set by the caller
        scope.launch {
            loadSuspend(model)
        }
    }

    /**
     * Unloads any model and then loads the provided model into RAM.
     */
    private suspend fun loadSuspend(model: LlmModel) {
        // assumes that _llmBusyLoading=true is already set by the caller

        val state = _modelStates[model]!!
        state.value = Loading

        curLoadedModel?.let { _modelStates[it]!!.value = NotLoaded }
        curLoadedModel = model
        prefs.edit { it[LAST_LOADED_LLAMACPP_MODEL] = model.filename }

        try {
            llamacpp.unload()
            llamacpp.load(File(filesDir, model.filename).path)
        } catch (e: IllegalStateException) {
            Log.e(TAG, "load() failed", e)
            state.value = ErrorLoading(e)
            return
        } finally {
            _busyLoading.value = false
        }

        state.value = Loaded
    }

    /**
     * @return the prompt format of the currently loaded model, or null if there is no loaded model
     * at the moment
     */
    fun getCurrentModel(): LlmModel? {
        return curLoadedModel
    }

    /**
     * @param prompt the prompt to send to the LLM
     * @param maxNewTokens the maximum number of new tokens that the LLM is allowed to generate
     * @param stopAtChar if provided, whenever the LLM produces this char as output, output
     *                   generation will be interrupted
     * @param temperature if `null` use deterministic greedy sampling, otherwise apply temperature
     *                    to have some variations in the output
     */
    fun getLlmResponse(
        prompt: String,
        maxNewTokens: Int,
        stopAtChar: Char?,
        temperature: Float?,
    ): Flow<String> {
        return llamacpp.send(prompt, maxNewTokens, stopAtChar, temperature)
    }

    companion object {
        fun <T> Iterable<Flow<T>>.combineToList(): Flow<List<T>> {
            var result = flowOf(listOf<T>())
            for (flow in this) {
                result = result.combine(flow) { list, newItem -> list.plus(newItem) }
            }
            return result
        }

        private val TAG = LlamacppManager::class.simpleName
    }
}
