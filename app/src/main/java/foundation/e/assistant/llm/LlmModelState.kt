/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.llm

/**
 * This is an enum with different fields depending on the current state, to avoid having nullable
 * objects all over the place in [LlamacppManager].
 */
sealed class LlmModelState {
    /**
     * The model is not present on disk.
     */
    data object NotDownloaded : LlmModelState()

    data class Downloading(
        val currentBytes: Long,
        val totalBytes: Long,
    ) : LlmModelState()

    data class ErrorDownloading(
        val throwable: Throwable
    ) : LlmModelState()

    /**
     * The model was already downloaded and is present on disk, however it has not been loaded yet.
     */
    data object NotLoaded : LlmModelState()

    data object Loading : LlmModelState()

    data class ErrorLoading(
        val throwable: Throwable
    ) : LlmModelState()

    /**
     * The model is loaded in RAM and ready to be used.
     */
    data object Loaded : LlmModelState()
}
