/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.llm.model

/**
 * Prompts are of this form usually:
 * ```
 * SYSTEM: general instructions on how the model is supposed to answer to questions
 * QUERY: a user query
 * RESPONSE: a response to the user query
 * QUERY: another user query
 * RESPONSE: another response to the user query
 * ...
 * QUERY: the user query to which the LLM should respond right after
 * RESPONSE:
 * ```
 */
data class PromptFormat(
    val systemPrefix: String,
    val systemSuffix: String,
    val queryPrefix: String,
    val querySuffix: String,
    val responsePrefix: String,
    val responsePrefixIfPresent: String,
    val responseSuffix: String,
) {
    fun formatPrompt(
        system: String?,
        examples: Iterable<Pair<String, String>>,
        actualQuery: String,
    ): String {
        val sb = StringBuilder()
        if (system != null) {
            sb.append(systemPrefix).append(system).append(systemSuffix)
        }
        for ((query, response) in examples) {
            sb.append(queryPrefix).append(query).append(querySuffix)
            sb.append(responsePrefixIfPresent).append(response).append(responseSuffix)
        }
        sb.append(queryPrefix).append(actualQuery).append(querySuffix)
        sb.append(responsePrefix) // response will be filled in by the LLM
        return sb.toString()
    }
}
