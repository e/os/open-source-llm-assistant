/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.llm.model


data class LlmModel(
    /**
     * The human readable name of the model (e.g. Phi-2).
     */
    val name: String,
    /**
     * The number of parameters, e.g. weights and biases, in the model.
     */
    val parameters: Long,
    /**
     * How the model was quantized (usually a short identifier).
     */
    val quantization: String,
    /**
     * Where the model can be downloaded.
     */
    val downloadUrl: String,
    /**
     * The size of the file to download, can be obtained by manually issuing a HEAD request to the
     * [downloadUrl].
     */
    val fileSizeBytes: Long,
    /**
     * The name of the file on disk, must be unique since the check for whether the model is already
     * downloaded is performed by checking if the corresponding file exists.
     */
    val filename: String,
    /**
     * The prompt format specific to this model. Note that an incorrect prompt format can heavily
     * impact the LLM performance, so make sure to use the format suggested by the model authors.
     */
    val promptFormat: PromptFormat,
    /**
     * If a model has been fine-tuned on generating JSON for skills, then there's no need to provide
     * the model with a lot of examples, since it already knows it should generate JSON to invoke a
     * skill, possibly with some captures.
     */
    val fineTunedForSkillJson: Boolean,
)

@Suppress("detekt:style:MaxLineLength") // links are long...
val LLM_MODELS = listOf(
    LlmModel(
        name = "Phi-2 finetuned",
        parameters = 2_780_000_000,
        quantization = "Q4_0",
        downloadUrl = "https://huggingface.co/Stypox/phi-2-finetuned-gguf/resolve/main/ggml-model-Q4_0.gguf?download=true",
        fileSizeBytes = 1602468384,
        filename = "phi-2-finetuned-q4_0.gguf",
        promptFormat = PromptFormat(
            systemPrefix = "System: ",
            systemSuffix = "\n\n",
            queryPrefix = "Instruct: ",
            querySuffix = "\n",
            responsePrefix = "Output:",
            responsePrefixIfPresent = "Output: ",
            responseSuffix = "\n\n",
        ),
        fineTunedForSkillJson = true,
    ),
    LlmModel(
        name = "Phi-2",
        parameters = 2_780_000_000,
        quantization = "Q4_0",
        downloadUrl = "https://huggingface.co/ggml-org/models/resolve/main/phi-2/ggml-model-q4_0.gguf?download=true",
        fileSizeBytes = 1602461536,
        filename = "phi-2-q4_0.gguf",
        promptFormat = PromptFormat(
            systemPrefix = "System: ",
            systemSuffix = "\n\n",
            queryPrefix = "Instruct: ",
            querySuffix = "\n",
            responsePrefix = "Output:",
            responsePrefixIfPresent = "Output: ",
            responseSuffix = "\n\n",
        ),
        fineTunedForSkillJson = false,
    ),
    LlmModel(
        name = "TinyLlama",
        parameters = 1_100_000_000,
        quantization = "f16",
        downloadUrl = "https://huggingface.co/ggml-org/models/resolve/main/tinyllama-1.1b/ggml-model-f16.gguf?download=true",
        fileSizeBytes = 2201990016,
        filename = "tinyllama-1.1-f16.gguf",
        promptFormat = PromptFormat(
            systemPrefix = "<|system|>\n",
            systemSuffix = "</s>\n",
            queryPrefix = "<|user|>\n",
            querySuffix = "</s>\n",
            responsePrefix = "<|assistant|>\n",
            responsePrefixIfPresent = "<|assistant|>\n",
            responseSuffix = "</s>\n",
        ),
        fineTunedForSkillJson = false,
    ),
    LlmModel(
        name = "Phi-2 DPO",
        parameters = 2_780_000_000,
        quantization = "Q3_K_M",
        downloadUrl = "https://huggingface.co/TheBloke/phi-2-dpo-GGUF/resolve/main/phi-2-dpo.Q3_K_M.gguf?download=true",
        fileSizeBytes = 1477279648,
        filename = "phi-2-dpo.Q3_K_M.gguf",
        promptFormat = PromptFormat(
            systemPrefix = "### System: ",
            systemSuffix = "\n\n",
            queryPrefix = "### Human: ",
            querySuffix = "\n\n",
            responsePrefix = "### Assistant:",
            responsePrefixIfPresent = "### Assistant: ",
            responseSuffix = "\n\n",
        ),
        fineTunedForSkillJson = false,
    ),
)
