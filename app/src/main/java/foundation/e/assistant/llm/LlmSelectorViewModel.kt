/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.llm

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import foundation.e.assistant.di.LAST_LOADED_LLAMACPP_MODEL
import foundation.e.assistant.llm.model.LlmModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Does not do much, as most of the work is performed by [LlamacppManager]. Does however handle
 * loading the last selected model from settings, and then keeping track of the selected model in
 * the menu, among those provided by [LlamacppManager].
 */
@HiltViewModel
class LlmSelectorViewModel @Inject constructor(
    application: Application,
    private val llamacppManager: LlamacppManager, // <- actually handles everything
    private val prefs: DataStore<Preferences>,
) : AndroidViewModel(application) {
    private val _selectedModel = MutableStateFlow<LlmModel?>(null)
    val selectedModel: StateFlow<LlmModel?> = _selectedModel

    val modelListStates get() = llamacppManager.modelListStates
    val busyLoading get() = llamacppManager.busyLoading

    init {
        viewModelScope.launch {
            val lastLoadedModelFilename = prefs.data.first()[LAST_LOADED_LLAMACPP_MODEL]
            val lastLoadedModel = modelListStates.value.find {
                it.first.filename == lastLoadedModelFilename
            }?.first
            _selectedModel.compareAndSet(null, lastLoadedModel)
        }
    }

    fun onSelectModel(model: LlmModel) {
        _selectedModel.value = model
    }

    fun onModelClick(model: LlmModel) {
        llamacppManager.advanceToNextState(model)
    }
}
