/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.input

import android.Manifest
import android.content.pm.PackageManager
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Download
import androidx.compose.material.icons.filled.Error
import androidx.compose.material.icons.filled.FolderZip
import androidx.compose.material.icons.filled.Mic
import androidx.compose.material.icons.filled.MicNone
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import foundation.e.assistant.R
import foundation.e.assistant.input.VoskUiState.Downloaded
import foundation.e.assistant.input.VoskUiState.Downloading
import foundation.e.assistant.input.VoskUiState.ErrorDownloading
import foundation.e.assistant.input.VoskUiState.ErrorLoading
import foundation.e.assistant.input.VoskUiState.ErrorUnzipping
import foundation.e.assistant.input.VoskUiState.Listening
import foundation.e.assistant.input.VoskUiState.Loaded
import foundation.e.assistant.input.VoskUiState.Loading
import foundation.e.assistant.input.VoskUiState.NoMicrophonePermission
import foundation.e.assistant.input.VoskUiState.NotDownloaded
import foundation.e.assistant.input.VoskUiState.NotLoaded
import foundation.e.assistant.input.VoskUiState.Unzipping
import foundation.e.assistant.util.LoadingProgress
import foundation.e.assistant.util.SmallCircularProgressIndicator
import foundation.e.assistant.util.loadingProgressString
import java.io.IOException

/**
 * Calls [VoskFab] with the data from the view model, and handles the microhone permission.
 */
@Composable
fun VoskFab() {
    val voskButtonViewModel: VoskButtonViewModel = viewModel()
    val state = voskButtonViewModel.state.collectAsState().value

    var microphonePermissionGranted by remember { mutableStateOf(true) }
    val context = LocalContext.current
    LaunchedEffect(null) {
        microphonePermissionGranted =
            ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) ==
                    PackageManager.PERMISSION_GRANTED
    }

    val launcher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        microphonePermissionGranted = isGranted
    }

    VoskFab(
        state = if (microphonePermissionGranted) state else NoMicrophonePermission,
        onClick = if (microphonePermissionGranted)
            voskButtonViewModel::onClick
        else
            { -> launcher.launch(Manifest.permission.RECORD_AUDIO) },
    )
}

/**
 * Renders a multi-use [FloatingActionButton] that shows the current Vosk state, and allows to
 * perform corresponding actions (downloading/unzipping/loading/listening) when pressed.
 */
@Composable
private fun VoskFab(state: VoskUiState, onClick: () -> Unit) {
    val text = voskFabText(state)
    var lastNonEmptyText by remember { mutableStateOf(text) }
    LaunchedEffect(text) {
        if (text != lastNonEmptyText && text.isNotEmpty()) {
            lastNonEmptyText = text
        }
    }

    // an ExtendedFloatingActionButton, but with animations and unexpanded form
    FloatingActionButton(
        onClick = onClick,
    ) {
        Row(
            modifier = Modifier
                .padding(start = 16.dp, end = if (text.isEmpty()) 16.dp else 22.dp)
                .animateContentSize(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            VoskFabIcon(state, contentDescription = text)
            if (text.isNotEmpty()) {
                Spacer(Modifier.width(16.dp))
                Text(lastNonEmptyText, color = MaterialTheme.colors.onSecondary)
            }
        }
    }
}

@Composable
private fun voskFabText(state: VoskUiState): String {
    return when (state) {
        NoMicrophonePermission -> stringResource(R.string.grant_microphone_permission)
        NotDownloaded -> stringResource(R.string.download_vosk)
        is Downloading -> loadingProgressString(
            LocalContext.current,
            state.currentBytes,
            state.totalBytes,
        )
        is ErrorDownloading -> stringResource(R.string.error_downloading)
        Downloaded -> stringResource(R.string.unzip_vosk)
        is Unzipping -> stringResource(R.string.unzipping)
        is ErrorUnzipping -> stringResource(R.string.error_unzipping)
        NotLoaded -> ""
        is Loading -> ""
        is ErrorLoading -> stringResource(R.string.error_loading)
        is Loaded -> ""
        is Listening -> stringResource(R.string.listening)
    }
}

@Composable
private fun VoskFabIcon(state: VoskUiState, contentDescription: String) {
    when (state) {
        NoMicrophonePermission -> Icon(Icons.Default.Warning, contentDescription)
        NotDownloaded -> Icon(Icons.Default.Download, contentDescription)
        is Downloading -> LoadingProgress(state.currentBytes, state.totalBytes)
        is ErrorDownloading -> Icon(Icons.Default.Error, contentDescription)
        Downloaded -> Icon(Icons.Default.FolderZip, contentDescription)
        is Unzipping -> LoadingProgress(state.currentBytes, state.totalBytes)
        is ErrorUnzipping -> Icon(Icons.Default.Error, contentDescription)
        NotLoaded -> Icon(Icons.Default.MicNone, stringResource(R.string.start_listening))
        is Loading -> if (state.thenStartListening)
            SmallCircularProgressIndicator()
        else // show the microphone if the model is loading but is not going to listen
            Icon(Icons.Default.MicNone, stringResource(R.string.start_listening))
        is ErrorLoading -> Icon(Icons.Default.Error, contentDescription)
        is Loaded -> Icon(Icons.Default.MicNone, stringResource(R.string.start_listening))
        is Listening -> Icon(Icons.Default.Mic, contentDescription)
    }
}

@Suppress("detekt:style:MagicNumber")
class VoskStatesPreviews : CollectionPreviewParameterProvider<VoskUiState>(listOf(
    NoMicrophonePermission,
    NotDownloaded,
    Downloading(987654, 0),
    Downloading(987654, 1234567),
    ErrorDownloading(IOException("ErrorDownloading exception")),
    Downloaded,
    Unzipping(765432, 0),
    Unzipping(765432, 9876543),
    ErrorUnzipping(Exception("ErrorUnzipping exception")),
    NotLoaded,
    Loading(true),
    Loading(false),
    ErrorLoading(Exception("ErrorLoading exception")),
    Loaded,
    Listening,
))

@Preview
@Composable
private fun VoskFabPreview(@PreviewParameter(VoskStatesPreviews::class) state: VoskUiState) {
    Column {
        Text(
            text = state.toString(),
            maxLines = 1,
            fontSize = 9.sp,
            color = Color.Black,
            modifier = Modifier
                .padding(bottom = 8.dp)
                .background(Color.White.copy(alpha = 0.5f))
                .width(256.dp)
        )
        VoskFab(
            state = state,
            onClick = {},
        )
    }
}
