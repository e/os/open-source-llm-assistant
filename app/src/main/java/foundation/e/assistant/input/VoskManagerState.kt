/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.assistant.input

import org.vosk.android.SpeechService


/**
 * The internal state for [VoskManager]. This is an enum with different fields depending on the
 * current state, to avoid having nullable objects all over the place in [VoskManager].
 * [VoskUiState] is symmetrical to this enum, except that it does not expose implementation-defined
 * fields to the UI, such as [SpeechService].
 */
sealed class VoskManagerState {
    /**
     * The model is not present on disk, neither in unzipped and in zipped form.
     */
    data object NotDownloaded : VoskManagerState()

    data class Downloading(
        val currentBytes: Long,
        val totalBytes: Long,
    ) : VoskManagerState()

    data class ErrorDownloading(
        val throwable: Throwable
    ) : VoskManagerState()

    data object Downloaded : VoskManagerState()

    /**
     * Vosk models are distributed in Zip files that need unzipping to be ready.
     */
    data class Unzipping(
        val currentBytes: Long,
        val totalBytes: Long,
    ) : VoskManagerState()

    data class ErrorUnzipping(
        val throwable: Throwable
    ) : VoskManagerState()

    /**
     * The model is present on disk, but was not loaded in RAM yet.
     */
    data object NotLoaded : VoskManagerState()

    /**
     * The model is being loaded, and [thenStartListening] indicates whether once loading is
     * finished, the STT should start listening right away.
     */
    data class Loading(
        val thenStartListening: Boolean
    ) : VoskManagerState()

    data class ErrorLoading(
        val throwable: Throwable
    ) : VoskManagerState()

    /**
     * The model, stored in [SpeechService], is ready in RAM, and can start listening at any time.
     */
    data class Loaded(
        internal val speechService: SpeechService
    ) : VoskManagerState()

    /**
     * The model, stored in [SpeechService], is listening.
     */
    data class Listening(
        internal val speechService: SpeechService
    ) : VoskManagerState()

    /**
     * Converts this [VoskManagerState] to a [VoskUiState], which is basically the same, except that
     * implementation-defined fields (e.g. [SpeechService]) are stripped away.
     */
    fun toUiState(): VoskUiState {
        return when (this) {
            NotDownloaded -> VoskUiState.NotDownloaded
            is Downloading -> VoskUiState.Downloading(currentBytes, totalBytes)
            is ErrorDownloading -> VoskUiState.ErrorDownloading(throwable)
            Downloaded -> VoskUiState.Downloaded
            is Unzipping -> VoskUiState.Unzipping(currentBytes, totalBytes)
            is ErrorUnzipping -> VoskUiState.ErrorUnzipping(throwable)
            NotLoaded -> VoskUiState.NotLoaded
            is Loading -> VoskUiState.Loading(thenStartListening)
            is ErrorLoading -> VoskUiState.ErrorLoading(throwable)
            is Loaded -> VoskUiState.Loaded
            is Listening -> VoskUiState.Listening
        }
    }
}
